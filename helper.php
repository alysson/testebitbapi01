<?php

function new_ev($service, $data=NULL){
    /*
    $event = new Google_Service_Calendar_Event(array(
        'summary' => 'aula gaal ' . date('d/m H:i:s'),
        'location' => 'Travessa Sagrado Coração de Jesus, 31',
        'description' => "Álgebra Vetorial\n
        Retas e Planos\n
        Matrizes, Sistemas Lineares e Determinantes\n
        O Espaço Vetorial Rn\n
        Autovalores e Autovetores de Matrizes\n
        Diagonalização de Matrizes Simétricas.",
        'start' => array(
            'dateTime' => date('c', strtotime('tomorrow 10am')),
            'timeZone' => 'America/Sao_Paulo',
            ),
        'end' => array(
            'dateTime' => date('c', strtotime('tomorrow 11:50am')),
            'timeZone' => 'America/Sao_Paulo',
        ),
        'attendees' => array(
            array('email' => 'alysson@caixaalta.net'),
            array('email' => 'brunaferreira422@gmail.com'),
            #array('email' => 'alss.ajaks@gmail.com'),
            #array('email' => 'alyssonajackson@icloud.com'),
            #array('email' => 'alss.web@gmail.com'),
        ),
    ));
    */

    $event = new Google_Service_Calendar_Event(array(
        'summary' => 'consulta paciente xxx' . date('d/m H:i:s'),
        'location' => 'Rua Jucá Miranda, 429 - Jardim São Luiz, Montes Claros - MG / 39401-507',
        'description' => "Consulta psi\nteste 01",
        'start' => array(
            'dateTime' => date('c', strtotime('tomorrow 10am')),
            'timeZone' => 'America/Sao_Paulo',
            ),
        'end' => array(
            'dateTime' => date('c', strtotime('tomorrow 11:50am')),
            'timeZone' => 'America/Sao_Paulo',
        ),
        'attendees' => array(
            array('email' => 'alysson@caixaalta.net'),
            array('email' => 'francis@engenhariadesistemas.com'),
            #array('email' => 'alss.ajaks@gmail.com'),
            #array('email' => 'alyssonajackson@icloud.com'),
            #array('email' => 'alss.web@gmail.com'),
        ),
    ));

    $options = [
        'sendNotifications' => TRUE,
    ];

    $CAL_ID = "caixaalta.net_va48evtnuo3r5apm2rq7g1s2r4@group.calendar.google.com";

    $event = $service->events->insert($CAL_ID, $event, $options);

    echo '<pre>';
    print_r($event);
    printf('Event created: %s\n', $event->htmlLink);

}
