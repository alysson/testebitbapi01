<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/helper.php';

session_start();

if(isset($_GET['d'])){
    session_destroy();
    unset($_SESSION['access_token']);
}

define('CAL_ID', 'caixaalta.net_2nk24dk9jtgujclp41g81qruac@group.calendar.google.com');

$client = new Google_Client();
$client->setAuthConfig('client_secrets.json');

#$client->addScope(Google_Service_Calendar::CALENDAR_READONLY);
$client->addScope(Google_Service_Calendar::CALENDAR);

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);

  // Print the next 10 events on the user's calendar.
  $calendarId = 'primary';
  $optParams = array(
    'maxResults' => 10,
    'orderBy' => 'startTime',
    'singleEvents' => TRUE,
    'timeMin' => date('c', strtotime('yesterday')),
  );
  
  $service = new Google_Service_Calendar($client);

  new_ev($service);

  $results = $service->events->listEvents(CAL_ID, $optParams);

  if (count($results->getItems()) == 0) {
    print "No upcoming events found.\n";
  } else {
    print "Upcoming events:\n";
    foreach ($results->getItems() as $event) {
        #var_dump($event);
      #$service->events->delete(CAL_ID, $event->id);
      $start = $event->start->dateTime;
      if (empty($start)) {
        $start = $event->start->date;
      }
      printf("%s (%s)<br>", $event->getSummary(), $start);
    }
  }

} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/cal/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
